/* Компонент для ввода части банковской карты */
Vue.component('card-input', {
    template: `<input 
        class="input"  
        :class="{error: error}"
        v-model="cardNumber"
        v-on:change="check()"
        required
    >`,
    props: ['maxlen'],
    data: function() {
        return {
            cardNumber: null,
            error: false
        }
    },
    methods: {
        check() {
            (/^\d+$/.test(this.cardNumber) && this.cardNumber.length == this.maxlen) ?
                this.error = false :
                this.error = true;
                this.$emit('enter-number', this.cardNumber, this.error)
        }
    }
})

/* Компонент для ввода имени */
Vue.component('name-input', {
    template:  `<input 
        type="text" 
        class="input" 
        placeholder="Держатель карты" 
        :class="{error: error}"
        v-model="cardholder"
        v-on:change="check"
        required
    >`,
    data: () => {
        return {
            cardholder: null,
            error: false
        }
    },
    methods: {
        check() {
            let name = this.cardholder.split(' ').join('');
            (name == name.replace(/[^a-zA-Z]/gi,'') &&
                name.length >= 4 )  ?
                this.error = false :
                this.error = true;
                this.$emit('enter-cardholder', this.cardholder, this.error)
        }
    }
})

/* Описание экземпляра Vue */
let card = new Vue({
    el: '#wrapper',
    data: {
        show: false,
        cardNumber: [],
        cardholder: '',
        code: '',
        date: {
            month: null,
            year: null
        },
        hasError: null
    },
    methods: {
        menuShow: function() {
            this.show = !this.show;
        },
        checkForm: function(e) {
            e.preventDefault();
        },
        submit() {
            /* Если в data объекты имеют значения, которые присвоятся, если они прошли валидацию, 
               то форма будет отправлена */
            if (this.hasError === false && 
                this.cardNumber.join('').length == 16 &&
                this.date.month && 
                this.date.year &&
                this.code && 
                this.cardholder) {
                if(this.cardNumber.join('').length == 16) {
                    console.log(`
                    Номер карты: ${this.cardNumber}\n
                    Срок действия: ${this.date.month + "/ " + this.date.year}\n
                    Владелец карты: ${this.cardholder}\n
                    Код: ${this.code}
                    `)
                    alert(`
                    Номер карты: ${this.cardNumber}\n
                    Срок действия: ${this.date.month + "/ " + this.date.year}\n
                    Владелец карты: ${this.cardholder}\n
                    Код: ${this.code}
                    `)
                }
            }
        },

        /* Получение части номера карты из каждого инпута */
        /* Передаем информацию, если содержится ошибка */
        submitOne(number, error) {
            this.cardNumber[0] = number;
            this.checkError(error)
        },
        submitTwo(number, error) {
            this.cardNumber[1] = number;
            this.checkError(error)
        },
        submitThree(number, error) {
            this.cardNumber[2] = number;
            this.checkError(error)
        },
        submitFour(number, error) {
            this.cardNumber[3] = number;
            this.checkError(error)
        },

        /* получаем имя */
        submitName(name, error) {
            this.cardholder = name;
            this.checkError(error)
        },

        /* Получаем дату */
        submitDate(date){
            this.date = date
            for(let key in date) {
                if (date[key] === null) {
                    this.error = true
                }
            }
        },
        /* Получаем код */
        submitCode(code, error) {
            this.code = code;
            this.checkError(error)
        },

        checkError(error) {
            error == false ?
                this.hasError = false :
                this.hasError = true;
        }
    },
})